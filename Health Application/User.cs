﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Health_Application
{
    //Author: Anais Zulfequar
    class User
    {
        //implement the gender field variable
        private string _gender;
        //implement the weight field variable
        private int _weight;
        //add the weight goal feild variable
        private int _weightGoal;
        //add the name field variable
        private string _name;
        
        //implement the properties for each field variable
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public int WeightGoal
        {
            get { return _weightGoal; }
            set { _weightGoal = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        //implement the read-only properties
        
        //weight to lose property to calculate how much weight the user needs to lose from his original weight
        public int WeightToLose
        {
            get { return _weight - _weightGoal; }
        }
        //weight to gain property to calculate how much weight the user needs to gain from his original weight
        public int WeightToGain
        {
            get { return _weightGoal - _weight; }
        }
    }
}
